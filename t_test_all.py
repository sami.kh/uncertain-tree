import numpy as np
import pandas as pd
from scipy.stats import norm
from scipy.stats import ttest_ind
from scipy.stats import wilcoxon


if __name__ == '__main__':

    alpha = 0.05
    dataset = 'ailerons'
    files = ['Error/GBT100_SGBT100_BooST100/' + dataset + '_mse_error.csv',
             'Error/GBT100_SGBT100_BooST100/' + dataset + '_smooth_error.csv',
             'Error/GBT100_SGBT100_BooST100/' + dataset + '_boost_error.csv']

    t_test_mat = np.ones((len(files), len(files)))
    boolean_ttest_mat = np.ones((len(files), len(files)))
    wilcoxon_mat = np.ones((len(files), len(files)))
    boolean_wilcoxon_mat = np.ones((len(files), len(files)))

    X = []
    Y = []
    for i in range(len(files)):
        for j in range(i+1, len(files)):
            with open(files[i], 'r') as file1:
                X = [float(v) for v in file1.readlines()]
            with open(files[j], 'r') as file2:
                Y = [float(v) for v in file2.readlines()]

            X = np.array(X)
            Y = np.array(Y)
            if len(X) != len(Y):
                raise Exception('Not the same size')
            _, p = ttest_ind(X, Y)
            t_test_mat[i, j] = t_test_mat[j, i] = p
            boolean_ttest_mat[i, j] = boolean_ttest_mat[j, i] = p > alpha
            # print('T Test', files[i], files[j], p > alpha, p)
            # _, p = wilcoxon(X, Y)
            # wilcoxon_mat[i, j] = wilcoxon_mat[j, i] = p
            # boolean_wilcoxon_mat[i, j] = boolean_wilcoxon_mat[j, i] = p > alpha
            # print('Wilcoxon Test', files[i], files[j], p > alpha, p)

    # pd.DataFrame(t_test_mat).to_csv('Error/t_test_tunned_noisy/' + ds + '_t_test_all.csv', header=None, index=None)
    # pd.DataFrame(wilcoxon_mat).to_csv('Error/k5/t_test/' + ds + '_wilcoxon_all.csv', header=None, index=None)
    print(t_test_mat)
    print()
    print(boolean_ttest_mat)
    # print()
    # print(wilcoxon_mat)
    # print()
    # print(boolean_wilcoxon_mat)
