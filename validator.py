import sys
import csv
import pandas as pd
from sklearn import tree
import numpy as np
from sklearn.model_selection import train_test_split
import argparse


# Main uncertain trees
if __name__ == '__main__':

    parser = argparse.ArgumentParser(description='Welcome to TDT builder')
    parser.add_argument('-x', type=str, help='x path')
    parser.add_argument('-y', type=str, help='y path')
    parser.add_argument('-o', type=str, help='output file')
    parser.add_argument('-e', type=str, help='error file')
    parser.add_argument('-p', type=int, help='header', default=0)
    parser.add_argument('-i', type=int, help='index column', default=None)
    parser.add_argument('-n', type=int, help='normalizer', default=0)
    parser.add_argument('-t', type=int, help='target column', default=None)
    parser.add_argument('-s', type=str, help='splitting method', default='topk3')
    parser.add_argument('-m', type=str, help='criterion method', default='mseprob')
    parser.add_argument('-l', type=float, help='min leaf percentage', default=0.1)
    parser.add_argument('-d', type=int, help='max depth', default=None)
    parser.add_argument('-r', type=str, help='delimeter', default=',')
    parser.add_argument('-cvs', type=int, help='cross validation min/max range', default=0)
    parser.add_argument('-cve', type=int, help='cross validation max range', default=10)
    parser.add_argument('-ts', type=float, help='test size in percentage', default=0.2)
    args = parser.parse_args()

    args.x = 'dataset/clean/Diabetes.csv'
    X = pd.read_csv(args.x, header=None, index_col=None)
    X = X.values
    Y = X[:, -1]
    X = X[:, 0:X.shape[1] - 1]

    args.s = 'topk3'
    outputs = []
    times = []
    rmses = []
    all_errors = []
    all_predictions = []
    all_y = []
    sizes = []
    sigma_values = [1e-10, 0.25, 0.5, 0.75, 1, 1.25, 1.5, 1.75, 2]
    # sigma_values = [0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0]

    for k in range(args.cvs, args.cve):
        X_train, X_test, y_train, y_test = train_test_split(X, Y, test_size=args.ts, random_state=k)
        print(k)
        sigma_Xp = np.std(X_train, axis=0)

        min_error = sys.float_info.max
        errors_arr = []
        regressor_arr = []

        X_tr_valid, X_ts_valid, y_tr_valid, y_ts_valid = train_test_split(X_train, y_train,
                                                                          test_size=args.ts, random_state=0)
        temp_min_smp_leaf = round(len(X_tr_valid) * args.l)

        # Validation loop
        for sigma_val in sigma_values:

            # print('Sigma multiplier', sigma_val)

            sigma_arr = sigma_Xp * sigma_val
            if sigma_val == 0:
                temp_method = 'mse'
            else:
                temp_method = args.m
            if temp_method in ['mse', 'msepred']:
                regressor = tree.DecisionTreeRegressor(criterion=temp_method, random_state=0,
                                                       min_samples_leaf=temp_min_smp_leaf,
                                                       tol=sigma_arr)
            else:
                regressor = tree.DecisionTreeRegressor(criterion=temp_method, random_state=0, splitter=args.s,
                                                       min_samples_leaf=temp_min_smp_leaf, tol=sigma_arr)
            regressor.fit(X_tr_valid, y_tr_valid)

            if temp_method == 'mse':
                prediction = regressor.predict(X_ts_valid)
            else:
                F = [f for f in regressor.tree_.feature if f != -2]
                for s_current_node in range(len(F)):
                    for kk in range(s_current_node + 1, len(F)):
                        if F[s_current_node] == F[kk]:
                            F[kk] = -1
                F = np.array(F)
                prediction = regressor.predict3(X_ts_valid, F=F)

            error = abs(y_ts_valid - prediction)
            RMSE_test = np.sqrt(np.mean(error ** 2))
            print(sigma_val, RMSE_test)
            errors_arr.append(RMSE_test)
            regressor_arr.append(regressor)

        # Testing
        ranking_sigma = np.argsort(errors_arr)
        best_sigma = sigma_values[ranking_sigma[0]]
        sigma_arr = sigma_Xp * best_sigma
        # best_regressor = regressor_arr[ranking_sigma[0]]

        if best_sigma == 0:
            temp_method = 'mse'
        else:
            temp_method = args.m

        temp_min_smp_leaf = round(len(X_train) * args.l)
        if temp_method in ['mse', 'msepred']:
            regressor = tree.DecisionTreeRegressor(criterion=temp_method, random_state=0,
                                                   min_samples_leaf=temp_min_smp_leaf, tol=sigma_arr)
        else:
            regressor = tree.DecisionTreeRegressor(criterion=temp_method, random_state=0, splitter=args.s,
                                                   min_samples_leaf=temp_min_smp_leaf, tol=sigma_arr)
        regressor.fit(X_train, y_train)
        if temp_method == 'mse':
            prediction = regressor.predict(X_test)
        else:
            F = [f for f in regressor.tree_.feature if f != -2]
            for s_current_node in range(len(F)):
                for kk in range(s_current_node + 1, len(F)):
                    if F[s_current_node] == F[kk]:
                        F[kk] = -1
            F = np.array(F)
            prediction = regressor.predict3(X_test, F=F)
        error = abs(y_test - prediction)
        all_errors.extend(error)
        RMSE_test = np.sqrt(np.mean(error ** 2))
        print(best_sigma, RMSE_test)
        rmses.append(RMSE_test)
        sizes.append(regressor.tree_.node_count)

        """
        outputs.append([k, errors_arr, best_sigma, RMSE_test])
        with open(args.o, 'w') as csv_file:
            spam_writer = csv.writer(csv_file, delimiter=',', quotechar='|', quoting=csv.QUOTE_MINIMAL)
            for output in outputs:
                spam_writer.writerow(output)
        """

    all_errors = np.array(all_errors)
    print('MEAN:', np.mean(rmses))
    outputs.append(['Average MSE', np.mean(rmses)])
    outputs.append(['STD', np.std(rmses)])
    outputs.append(['size', np.mean(sizes)])
    outputs.append(['Total Average MSE', np.sqrt(np.mean(all_errors ** 2))])

    print()
    print('Ahoooooy')
    for rmse in rmses:
        print(rmse)
    print()
    print(np.mean(rmses))
    print(np.std(rmses))
    """
    with open(args.o, 'w') as csv_file:
        spam_writer = csv.writer(csv_file, delimiter=',', quotechar='|', quoting=csv.QUOTE_MINIMAL)
        for output in outputs:
            spam_writer.writerow(output)

    all_errors = all_errors.T
    pd.DataFrame(all_errors).to_csv(args.e, index=False, header=False)
    """
