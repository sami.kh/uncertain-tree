import numpy as np
import pandas as pd
import random
import time
import csv
import argparse
from pyearth import Earth
from sklearn.model_selection import train_test_split

import warnings

warnings.simplefilter(action='ignore', category=FutureWarning)

if __name__ == '__main__':

    parser = argparse.ArgumentParser(description='Welcome to TDT builder')
    parser.add_argument('-x', type=str, help='x path')
    parser.add_argument('-o', type=str, help='output file')
    parser.add_argument('-e', type=str, help='error file')
    parser.add_argument('-cvs', type=int, help='cross validation min/max range', default=0)
    parser.add_argument('-cve', type=int, help='cross validation max range', default=10)

    args = parser.parse_args()
    args.x = 'dataset/clean/Ozone.csv'
    X = pd.read_csv(args.x, header=None, index_col=None)
    X = X.values
    Y = X[:, -1]
    X = X[:, 0:X.shape[1] - 1]
    outputs = []
    times = []
    rmses = []
    all_errors = []
    learning_rate = 0.1
    for k in range(args.cvs, args.cve):
        X_train, X_test, y_train, y_test = train_test_split(X, Y, test_size=0.2, random_state=k)
        out = np.zeros(len(y_train))
        out.fill(np.average(y_train))
        predictions = np.zeros(len(y_test))
        predictions.fill(np.average(y_test))
        mars_estimators = np.empty(50, dtype=object)
        for j in range(50):
            print(k, j)
            residuals = y_train - out
            model = Earth()
            model.fit(X_train, residuals)
            mars_estimators[j] = model
            y_hat = model.predict(X_train)
            out += learning_rate * y_hat

        for j in range(50):
            model = mars_estimators[j]
            predictions += learning_rate * model.predict(X_test)

        error = np.abs(y_test - predictions)
        all_errors.extend(error)
        RMSE_test = np.sqrt(np.mean(error ** 2))
        print('{:.2f}'.format(RMSE_test))
        rmses.append(RMSE_test)
        outputs.append([k, RMSE_test])
        """
        with open(args.o, 'w') as csv_file:
            spam_writer = csv.writer(csv_file, delimiter=',', quotechar='|', quoting=csv.QUOTE_MINIMAL)
            for output in outputs:
                spam_writer.writerow(output)
        """

    print('{:.2f}'.format(np.mean(rmses)))
    pd.DataFrame(all_errors).to_csv(args.e, index=False, header=False)
