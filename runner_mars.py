import numpy as np
import pandas as pd
import time
import csv
import argparse
from pyearth import Earth
from sklearn.model_selection import train_test_split

import warnings

warnings.simplefilter(action='ignore', category=FutureWarning)

if __name__ == '__main__':

    parser = argparse.ArgumentParser(description='Welcome to TDT builder')
    parser.add_argument('-x', type=str, help='x path')
    parser.add_argument('-o', type=str, help='output file')
    parser.add_argument('-e', type=str, help='error file')
    args = parser.parse_args()
    X = pd.read_csv(args.x, header=None, index_col=None)
    X = X.values
    Y = X[:, -1]
    X = X[:, 0:X.shape[1] - 1]
    all_errors = []
    times = []
    rmses = []
    outputs = []
    for k in range(10):
        X_train, X_test, y_train, y_test = train_test_split(X, Y, test_size=0.2, random_state=k)
        model = Earth()
        t = time.time()
        model.fit(X_train, y_train)
        t = time.time() - t
        y_hat = model.predict(X_test)
        error = abs(y_test - y_hat)
        all_errors.extend(error)
        RMSE_test = np.sqrt(np.mean(error ** 2))
        rmses.append(RMSE_test)
        times.append(t)
        print('{:.2f}'.format(RMSE_test), t)
        outputs.append([k, RMSE_test, t])

        with open(args.o, 'w') as csv_file:
            spam_writer = csv.writer(csv_file, delimiter=',', quotechar='|', quoting=csv.QUOTE_MINIMAL)
            for output in outputs:
                spam_writer.writerow(output)

    print('{:.2f}'.format(np.mean(rmses)), '{:.2f}'.format(np.mean(times)))
    pd.DataFrame(all_errors).to_csv(args.e, index=False, header=False)
