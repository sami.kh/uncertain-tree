import sys
import csv
import random
import pandas as pd
from sklearn import tree
from pyearth import Earth
import numpy as np
from sklearn.model_selection import train_test_split
import argparse
import warnings


# Main uncertain trees
if __name__ == '__main__':

    parser = argparse.ArgumentParser(description='Welcome to TDT builder')
    parser.add_argument('-x', type=str, help='x path')
    parser.add_argument('-y', type=str, help='y path')
    parser.add_argument('-o', type=str, help='output file')
    parser.add_argument('-e', type=str, help='error file')
    parser.add_argument('-p', type=int, help='header', default=0)
    parser.add_argument('-i', type=int, help='index column', default=None)
    parser.add_argument('-n', type=int, help='normalizer', default=None)
    parser.add_argument('-t', type=int, help='target column', default=None)
    parser.add_argument('-s', type=str, help='splitting method', default='topk3')
    parser.add_argument('-m', type=str, help='criterion method', default='mseprob')
    parser.add_argument('-l', type=float, help='min leaf percentage', default=0.1)
    parser.add_argument('-d', type=int, help='max depth', default=None)
    parser.add_argument('-r', type=str, help='delimeter', default=',')
    parser.add_argument('-sg', type=str, help='sigma values separated by , for each fold')
    parser.add_argument('-cvs', type=int, help='cross validation min/max range', default=0)
    parser.add_argument('-cve', type=int, help='cross validation max range', default=10)
    parser.add_argument('-ts', type=float, help='test size in percentage', default=0.2)
    args = parser.parse_args()

    warnings.filterwarnings("ignore")  # For EARTH/MARS
    args.m = 'mseprob'
    X = pd.read_csv(args.x, header=None, index_col=None)

    # Convert pandas into nd array
    X = X.values
    Y = X[:, -1]
    X = X[:, 0:X.shape[1] - 1]

    rmses = []
    all_errors = []
    all_errors_25 = []
    all_errors_50 = []
    sigmas = None
    if args.sg:
        sigmas = [float(v) for v in args.sg.split(',')]
    outputs = []
    # vars_indexes = list(range(X.shape[1]))

    for ind, k in enumerate(range(args.cvs, args.cve)):
        X_train, X_test, y_train, y_test = train_test_split(X, Y, test_size=args.ts, random_state=k)
        print(k)

        rf_predictions = []
        for j in range(100):
            print(j)
            # tr_size = random.randrange(62, 80) / 100
            # var_size = random.randrange(20, 30) / 100
            # var_size = round(var_size * len(vars_indexes))
            # var_size = max(var_size, 3)

            np.random.seed(j*k + j)
            idx = np.random.randint(len(X_train), size=len(X_train))
            X_subtree = X_train[idx, :]
            y_subtree = y_train[idx]
            sigma_Xp = np.std(X_subtree, axis=0) * 2

            if args.sg:
                sigma_Xp = sigma_Xp * sigmas[ind]
            temp_min_smp_leaf = round(len(X_subtree) * args.l)

            temp_method = args.m
            if args.sg:
                if sigmas[ind] == 0:
                    temp_method = 'mse'
            if temp_method in ['mse', 'msepred']:
                regressor = tree.DecisionTreeRegressor(criterion=temp_method, random_state=0,
                                                       min_samples_leaf=temp_min_smp_leaf, tol=sigma_Xp)
            elif temp_method == 'mseprob':
                regressor = tree.DecisionTreeRegressor(criterion=temp_method, random_state=0, splitter=args.s,
                                                       min_samples_leaf=temp_min_smp_leaf, tol=sigma_Xp)
            else:
                regressor = Earth()
            regressor.fit(X_subtree, y_subtree)

            # new_X_test = X_test[:, new_vars]
            if temp_method == 'mse':
                prediction = regressor.predict(X_test)
            elif temp_method == 'mseprob':
                F = [f for f in regressor.tree_.feature if f != -2]
                for s_current_node in range(len(F)):
                    for kk in range(s_current_node + 1, len(F)):
                        if F[s_current_node] == F[kk]:
                            F[kk] = -1
                F = np.array(F)
                prediction = regressor.predict3(X_test, F=F)
            else:
                prediction = regressor.predict(X_test)
            rf_predictions.append(prediction)

        prediction_25 = np.mean(rf_predictions[:25], axis=0)
        prediction_50 = np.mean(rf_predictions[:50], axis=0)
        prediction = np.mean(rf_predictions, axis=0)
        error_25 = abs(y_test - prediction_25)
        error_50 = abs(y_test - prediction_50)
        error = abs(y_test - prediction)
        all_errors.extend(error)
        all_errors_25.extend(error_25)
        all_errors_50.extend(error_50)
        RMSE_test_25 = np.sqrt(np.mean(error_25 ** 2))
        RMSE_test_50 = np.sqrt(np.mean(error_50 ** 2))
        RMSE_test = np.sqrt(np.mean(error ** 2))
        print(RMSE_test_25, RMSE_test_50, RMSE_test)
        rmses.append([RMSE_test_25, RMSE_test_50, RMSE_test])

        outputs.append([k, RMSE_test_25, RMSE_test_50, RMSE_test])

        with open(args.o, 'w') as csv_file:
            spam_writer = csv.writer(csv_file, delimiter=',', quotechar='|', quoting=csv.QUOTE_MINIMAL)
            for output in outputs:
                spam_writer.writerow(output)

    outputs.append([])
    all_errors = np.array(all_errors)
    all_errors_25 = np.array(all_errors_25)
    all_errors_50 = np.array(all_errors_50)
    outputs.append(['Average MSE'] + list(np.mean(rmses, axis=0)))
    outputs.append(['STD'] + list(np.std(rmses, axis=0)))
    outputs.append(['Total Average MSE', np.sqrt(np.mean(all_errors_25 ** 2)),
                    np.sqrt(np.mean(all_errors_50 ** 2)), np.sqrt(np.mean(all_errors ** 2))])

    with open(args.o, 'w') as csv_file:
        spam_writer = csv.writer(csv_file, delimiter=',', quotechar='|', quoting=csv.QUOTE_MINIMAL)
        for output in outputs:
            spam_writer.writerow(output)

    all_errors = all_errors.T
    all_errors_25 = all_errors_25.T
    all_errors_50 = all_errors_50.T
    pd.DataFrame(all_errors).to_csv(args.e, index=False, header=False)
    pd.DataFrame(all_errors_25).to_csv(args.e.replace('_error', '_error_25'), index=False, header=False)
    pd.DataFrame(all_errors_50).to_csv(args.e.replace('_error', '_error_50'), index=False, header=False)
