import numpy as np
import pandas as pd
import random
import time
import csv
from pyearth import Earth
from sklearn.model_selection import train_test_split
import argparse
import warnings

warnings.simplefilter(action='ignore', category=FutureWarning)

if __name__ == '__main__':

    parser = argparse.ArgumentParser(description='Welcome to TDT builder')
    parser.add_argument('-x', type=str, help='x path')
    parser.add_argument('-o', type=str, help='output file')
    parser.add_argument('-e', type=str, help='error file')
    parser.add_argument('-cvs', type=int, help='cross validation min/max range', default=0)
    parser.add_argument('-cve', type=int, help='cross validation max range', default=10)
    args = parser.parse_args()
    X = pd.read_csv(args.x, header=None, index_col=None)
    X = X.values
    Y = X[:, -1]
    X = X[:, 0:X.shape[1] - 1]
    all_errors = []
    all_errors_25 = []
    all_errors_50 = []
    times = []
    outputs = []
    rmses = []
    for k in range(args.cvs, args.cve):
        X_train, X_test, y_train, y_test = train_test_split(X, Y, test_size=0.2, random_state=k)
        rf_predictions = []
        for j in range(100):
            print(k, j)
            np.random.seed(j * k + j)
            idx = np.random.randint(len(X_train), size=len(X_train))
            X_subtree = X_train[idx, :]
            y_subtree = y_train[idx]
            model = Earth()
            model.fit(X_subtree, y_subtree)
            y_hat = model.predict(X_test)
            rf_predictions.append(y_hat)

        prediction_25 = np.mean(rf_predictions[:25], axis=0)
        prediction_50 = np.mean(rf_predictions[:50], axis=0)
        prediction = np.mean(rf_predictions, axis=0)
        error_25 = abs(y_test - prediction_25)
        error_50 = abs(y_test - prediction_50)
        error = abs(y_test - prediction)
        all_errors.extend(error)
        all_errors_25.extend(error_25)
        all_errors_50.extend(error_50)
        RMSE_test_25 = np.sqrt(np.mean(error_25 ** 2))
        RMSE_test_50 = np.sqrt(np.mean(error_50 ** 2))
        RMSE_test = np.sqrt(np.mean(error ** 2))
        print(RMSE_test_25, RMSE_test_50, RMSE_test)
        rmses.append([RMSE_test_25, RMSE_test_50, RMSE_test])
        outputs.append([k, RMSE_test_25, RMSE_test_50, RMSE_test])
        with open(args.o, 'w') as csv_file:
            spam_writer = csv.writer(csv_file, delimiter=',', quotechar='|', quoting=csv.QUOTE_MINIMAL)
            for output in outputs:
                spam_writer.writerow(output)

    pd.DataFrame(all_errors).to_csv(args.e, index=False, header=False)
