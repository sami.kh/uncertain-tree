import csv
import pandas as pd
from sklearn import ensemble
import numpy as np
import time
from sklearn.model_selection import train_test_split
import argparse


# Main uncertain trees
if __name__ == '__main__':

    parser = argparse.ArgumentParser(description='Welcome to TDT builder')
    parser.add_argument('-x', type=str, help='x path')
    parser.add_argument('-o', type=str, help='output file')
    parser.add_argument('-e', type=str, help='error file')
    parser.add_argument('-s', type=str, help='splitting method', default='topk3')
    parser.add_argument('-m', type=str, help='criterion method', default='mseprob')
    parser.add_argument('-n', type=int, help='number of estimator', default=100)
    parser.add_argument('-l', type=float, help='min leaf percentage', default=0.1)
    parser.add_argument('-d', type=int, help='max depth', default=None)
    parser.add_argument('-r', type=str, help='delimeter', default=',')
    parser.add_argument('-sg', type=str, help='sigma values separated by , for each fold', default=None)
    parser.add_argument('-cvs', type=int, help='cross validation min/max range', default=0)
    parser.add_argument('-cve', type=int, help='cross validation max range', default=10)
    parser.add_argument('-ts', type=float, help='test size in percentage', default=0.2)
    args = parser.parse_args()

    dataset = 'e2006_top9k'
    args.x = 'dataset/clean/' + dataset + '.csv'
    args.n = 50
    args.m = 'mse'
    args.o = 'results/std_gbt_100_tunned/' + dataset + '_50.csv'
    args.e = 'results/std_gbt_100_tunned/' + dataset + '_50_error.csv'

    X = pd.read_csv(args.x, header=None, index_col=None)

    # Convert pandas into nd array
    X = X.values
    Y = X[:, -1]
    X = X[:, 0:X.shape[1] - 1]

    outputs = []
    times = []
    rmses = []
    all_errors = []
    all_predictions = []
    all_y = []

    sigmas = None
    if args.sg is not None:
        sigmas = [float(v) for v in args.sg.split(',')]

    for ind, k in enumerate(range(args.cvs, args.cve)):

        X_train, X_test, y_train, y_test = train_test_split(X, Y, test_size=args.ts, random_state=k)
        min_samples_leaf = round(len(X_train) * args.l)

        # Presort needs to be False (for now)
        regressor = ensemble.GradientBoostingRegressor(random_state=0, n_estimators=args.n, presort=False,
                                                       min_samples_leaf=min_samples_leaf, criterion=args.m)
        t = time.process_time()

        if args.m == 'mse':
            regressor.fit(X_train, y_train)
        else:
            if sigmas is not None:
                regressor.fit_un(X_train, y_train, sigma_mult=sigmas[ind])
            else:
                regressor.fit_un(X_train, y_train)
        t = time.process_time() - t

        prediction = regressor.predict(X_test)

        all_predictions.extend(prediction)
        all_y.extend(y_test)
        error = abs(y_test - prediction)
        # mape_error = error / y_test
        all_errors.extend(error)
        RMSE_test = np.sqrt(np.mean(error ** 2))
        # mape = np.mean(mape_error) * 100
        print(k, 'RMSE', RMSE_test)
        # outputs.append([mape])
        outputs.append([k, RMSE_test, t])

        times.append(t)
        rmses.append(RMSE_test)

        with open(args.o, 'w') as csv_file:
            spam_writer = csv.writer(csv_file, delimiter=',', quotechar='|', quoting=csv.QUOTE_MINIMAL)
            for output in outputs:
                spam_writer.writerow(output)

    print()
    all_errors = np.array(all_errors)
    outputs.append(['Average MSE', np.mean(rmses)])
    outputs.append(['STD', np.std(rmses)])
    print('Average MSE', np.mean(rmses))
    print('Average Std', np.std(rmses))
    outputs.append(['Total Average MSE', np.sqrt(np.mean(all_errors ** 2))])

    with open(args.o, 'w') as csv_file:
        spam_writer = csv.writer(csv_file, delimiter=',', quotechar='|', quoting=csv.QUOTE_MINIMAL)
        for output in outputs:
            spam_writer.writerow(output)

    all_errors = all_errors.T
    pd.DataFrame(all_errors).to_csv(args.e, index=False, header=False)
