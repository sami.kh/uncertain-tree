import pandas as pd
from sklearn import tree
import numpy as np
from sklearn.model_selection import train_test_split
import argparse
import os


def get_string_rep(values):
    _min = np.min(values)
    _max = np.max(values)
    _mean = np.mean(values)
    _std = np.std(values)
    _median = np.median(values)
    _q_25 = np.quantile(values, 0.25)
    _q_75 = np.quantile(values, 0.75)

    return ';'.join(['{:.2f}'.format(_min), '{:.2f}'.format(_max), '{:.2f}'.format(_mean), '{:.2f}'.format(_std),
                     '{:.2f}'.format(_median), '{:.2f}'.format(_q_25), '{:.2f}'.format(_q_75)])


def get_leaves_paths(left_children, right_children, n_regions):

    leaves_paths = [[] for _ in range(n_regions)]
    leaves = np.where(left_children == -1)[0]

    for ind, leaf in enumerate(leaves):
        parent_id = leaf
        while parent_id != 0:
            if parent_id in left_children:
                parent_id = np.where(left_children == parent_id)[0][0]
                leaves_paths[ind].insert(0, 'L')
            else:
                parent_id = np.where(right_children == parent_id)[0][0]
                leaves_paths[ind].insert(0, 'R')
            leaves_paths[ind].insert(0, parent_id)

    prefixes = []
    seconds_nodes = []
    paths = []
    for first_node in reversed(range(n_regions - 1)):
        for second_node in reversed(range(first_node + 1, n_regions)):
            longest_prefix = os.path.commonprefix([leaves_paths[first_node], leaves_paths[second_node]])
            prefix = [first_node, len(longest_prefix)]
            if prefix not in prefixes and second_node not in seconds_nodes:
                paths.append(longest_prefix)
                prefixes.append(prefix)
                seconds_nodes.append(second_node)

    for ind, line in enumerate(prefixes):
        line.extend([seconds_nodes[ind], paths[ind]])
    return prefixes


# Main uncertain trees
if __name__ == '__main__':

    parser = argparse.ArgumentParser(description='Welcome to TDT builder')
    parser.add_argument('-x', type=str, help='x path')
    parser.add_argument('-y', type=str, help='y path')
    parser.add_argument('-o', type=str, help='output file')
    parser.add_argument('-e', type=str, help='error file')
    parser.add_argument('-p', type=int, help='header', default=0)
    parser.add_argument('-i', type=int, help='index column', default=None)
    parser.add_argument('-n', type=int, help='normalizer', default=None)
    parser.add_argument('-t', type=int, help='target column', default=None)
    parser.add_argument('-s', type=str, help='splitting method', default=None)
    parser.add_argument('-m', type=str, help='criterion method', default='mseprob')
    parser.add_argument('-l', type=float, help='min leaf percentage', default=0.1)
    parser.add_argument('-d', type=int, help='max depth', default=None)
    parser.add_argument('-r', type=str, help='delimeter', default=',')
    parser.add_argument('-cvs', type=int, help='cross validation min/max range', default=0)
    parser.add_argument('-cve', type=int, help='cross validation max range', default=10)
    parser.add_argument('-ts', type=float, help='test size in percentage', default=0.2)
    args = parser.parse_args()

    args.x = 'dataset/clean/Ozone.csv'
    args.s = 'topk3'
    # args.m = 'mse'

    if args.p:
        X = pd.read_csv(args.x, delimiter=args.r, index_col=args.i)
    else:
        X = pd.read_csv(args.x, header=None, delimiter=args.r, index_col=args.i)
    # Convert pandas into nd array

    X = X.values

    if args.y:
        if args.p:
            Y = pd.read_csv(args.y, delimiter=args.r, index_col=args.i)
        else:
            Y = pd.read_csv(args.y, header=None, delimiter=args.r, index_col=args.i)
        Y = Y.values
        Y = Y.ravel()
    else:
        if args.t:
            Y = X[:, 0]
            X = X[:, 1:X.shape[1]]
        else:
            Y = X[:, -1]
            X = X[:, 0:X.shape[1] - 1]

    X_train, X_test, y_train, y_test = train_test_split(X, Y, test_size=args.ts, random_state=42)

    sigma_Xp = np.std(X_train, axis=0)
    min_samples_leaf = round(len(X_train) * args.l)

    if args.m in ['mse', 'msepred']:
        regressor = tree.DecisionTreeRegressor(criterion=args.m, random_state=0,
                                               min_samples_leaf=min_samples_leaf, tol=sigma_Xp)
    else:
        regressor = tree.DecisionTreeRegressor(criterion=args.m, random_state=0, splitter=args.s, samples=None,
                                               min_samples_leaf=min_samples_leaf, tol=sigma_Xp)
    regressor.fit(X_train, y_train)

    prefixes = get_leaves_paths(regressor.tree_.children_left,
                                regressor.tree_.children_right, regressor.tree_.preg.shape[1])
    data_file = '@RELATION Ozone\n\n'
    data_file += '@ATTRIBUTE name string\n'

    all_clusters = None
    important_features = None
    X_arff = None

    region_line_dict = dict()
    classes_names = dict()
    for p_ind in range(1, regressor.tree_.preg.shape[1] + 1):
        region_line_dict[str(p_ind - 1)] = X_train.shape[0] * p_ind - p_ind
        classes_names[str(p_ind - 1)] = 'C' + str(p_ind - 1) + ';89'

    all_values_dict = {str(i): dict() for i in range(regressor.tree_.preg.shape[1])}

    if args.m == 'mse':
        prediction = regressor.predict(X_test)
    else:
        F = [f for f in regressor.tree_.feature if f != -2]
        for s_current_node in range(len(F)):
            for kk in range(s_current_node + 1, len(F)):
                if F[s_current_node] == F[kk]:
                    F[kk] = -1
        F = np.array(F)
        prediction = regressor.predict3(X_test, F=F)
        important_features = list([f for f in F if f != -1])
        for f in important_features:
            data_file += '@ATTRIBUTE V' + str(f) + ' numeric\n'
        data_file += '@ATTRIBUTE P numeric\n'
        data_file += '@ATTRIBUTE P1 numeric\n'
        data_file += '@ATTRIBUTE P2 numeric\n'
        data_file += '@ATTRIBUTE P3 numeric\n'

        sorted_preg = np.copy(regressor.tree_.preg)
        sorted_preg.sort(axis=1)
        preg_best_first_region = np.copy(regressor.tree_.preg)
        preg_best_second_region = np.copy(regressor.tree_.preg)
        preg_best_third_region = np.copy(regressor.tree_.preg)
        for i in range(regressor.tree_.preg.shape[0]):
            for j in range(regressor.tree_.preg.shape[1]):
                if preg_best_first_region[i, j] != sorted_preg[i, -1]:
                    preg_best_first_region[i, j] = 0
                if preg_best_second_region[i, j] != sorted_preg[i, -2]:
                    preg_best_second_region[i, j] = 0
                if preg_best_third_region[i, j] != sorted_preg[i, -3]:
                    preg_best_third_region[i, j] = 0

        new_length = len(X_train) * regressor.tree_.preg.shape[1]
        X_ids = np.arange(1, (new_length + 1)).reshape(new_length, 1)
        X_arff = X_train[:, important_features]
        X_arff = np.tile(X_arff, (regressor.tree_.preg.shape[1], 1))
        X_arff = np.concatenate((X_ids, X_arff), axis=1)

        preg_flatten = regressor.tree_.preg.flatten('F')
        X_arff = np.concatenate((X_arff, preg_flatten.reshape(len(X_arff), 1)), axis=1)

        preg_best_first_flatten = preg_best_first_region.flatten('F')
        X_arff = np.concatenate((X_arff, preg_best_first_flatten.reshape(len(X_arff), 1)), axis=1)

        preg_best_second_flatten = preg_best_second_region.flatten('F')
        X_arff = np.concatenate((X_arff, preg_best_second_flatten.reshape(len(X_arff), 1)), axis=1)

        preg_best_third_flatten = preg_best_third_region.flatten('F')
        X_arff = np.concatenate((X_arff, preg_best_third_flatten.reshape(len(X_arff), 1)), axis=1)

        preg_argmax = np.argmax(regressor.tree_.preg, axis=1)
        preg_argmax = np.tile(preg_argmax, (regressor.tree_.preg.shape[1], 1)).flatten('C')
        X_arff = np.concatenate((X_arff, preg_argmax.reshape(len(X_arff), 1)), axis=1)

        classes = '{' + ','.join(['"C' + str(c) + '"' for c in set(preg_argmax)]) + '}\n\n'
        data_file += '@ATTRIBUTE class ' + classes
        data_file += '@DATA\n'

        # Generating cluster.arff
        all_clusters = []
        row_index = 0
        prows_count = []
        class_dict = dict()
        p_ind = 0
        curr_index = 1

        for p_range in range(0, X_arff.shape[0], X_train.shape[0]):
            p_start = p_range
            p_end = p_range + X_train.shape[0]
            class_n_obs = 2
            pid_rows = X_arff[p_start: p_end, :]
            values = None
            for f in range(1, pid_rows.shape[1] - 1):
                pid_features_values = pid_rows[:, f]
                index_1, index_2 = int(-pid_rows[0, 0]), int(-pid_rows[1, 0])
                values = [pid_features_values[0], pid_features_values[1]]
                str_values = get_string_rep(values)
                if f == 1:
                    _class = 'C' + str(p_ind) + ';' + str(class_n_obs)
                    row_str_rep = ','.join(['"' + str(curr_index) + '"', str(index_1), str(index_2), str(0.0),
                                            _class, str_values])
                    all_clusters.append(row_str_rep)
                    row_index += 1
                    curr_index += 1
                    class_n_obs += 1
                else:
                    all_clusters[p_range - p_ind] += ',' + str_values

                for ind, val in enumerate(pid_features_values[2:]):
                    values.append(val)
                    str_values = get_string_rep(values)
                    if f == 1:
                        _class = 'C' + str(p_ind) + ';' + str(class_n_obs)
                        class_n_obs += 1
                        row_str_rep = ','.join(['"' + str(curr_index) + '"', str(int(-pid_rows[ind+2, 0])),
                                                str(row_index), str(0.0), _class, str_values])
                        all_clusters.append(row_str_rep)
                        row_index += 1
                        curr_index += 1
                    else:
                        all_clusters[p_range - p_ind + ind + 1] += ',' + str_values

                if (f - 1) >= len(important_features):
                    all_values_dict[str(p_ind)][f - 1] = values

            p_ind += 1

    nodes = regressor.tree_.__getstate__()['nodes']
    for row in sorted(prefixes, key=lambda x: x[1], reverse=True):

        temp_row_0 = row[0]
        temp_row_1 = row[2]
        for key, val in region_line_dict.items():
            if str(row[0]) in key.split(';'):
                temp_row_0 = key
            if str(row[2]) in key.split(';'):
                temp_row_1 = key

        p_ind_1 = region_line_dict[temp_row_0]
        p_ind_2 = region_line_dict[temp_row_1]
        distance = regressor.tree_.preg.shape[1] - row[1]
        classes = ';'.join([classes_names[temp_row_0], classes_names[temp_row_1]])

        p_cluster = None
        for f in range(len(important_features) + 4):
            values_1 = None
            if f >= len(important_features):
                values_1 = all_values_dict[str(temp_row_0)][f]
                values_2 = all_values_dict[str(temp_row_1)][f]
                values_1.extend(values_2)
                values_1 = [v for v in values_1 if v != 0]
                str_values = get_string_rep(values_1)
                if f == len(important_features):
                    p_cluster = str_values
                else:
                    p_cluster += ',' + str_values
                if ';'.join([temp_row_0, temp_row_1]) not in all_values_dict:
                    all_values_dict[';'.join([temp_row_0, temp_row_1])] = dict()
                all_values_dict[';'.join([temp_row_0, temp_row_1])][f] = values_1

        del all_values_dict[str(temp_row_0)]
        del all_values_dict[str(temp_row_1)]
        del classes_names[temp_row_0]
        del classes_names[temp_row_1]
        classes_names[';'.join([temp_row_0, temp_row_1])] = classes

        names = []
        for path_ind in range(0, len(row[3]), 2):
            name = ''
            var = row[3][path_ind]
            direction = 'P'
            if len(row[3]) > path_ind + 1:
                direction = row[3][path_ind + 1]
            name += 'V' + str(nodes[var][2])
            if direction == 'R':
                name += '>' + str('{:.3f}'.format(nodes[var][3]))
            if direction == 'L':
                name += '<=' + str('{:.3f}'.format(nodes[var][3]))
            if direction == 'P':
                name += '=' + str('{:.3f}'.format(nodes[var][3]))
            names.append(name)

        name = ';'.join(names)
        name = '"' + name + '"'

        temp_cluster = all_clusters[-1].split(',')[5: 5 + len(important_features)]
        temp_cluster = ','.join([name, str(p_ind_1), str(p_ind_2), str(distance), classes,
                                 ','.join(temp_cluster), p_cluster])
        all_clusters.append(temp_cluster)

        del region_line_dict[temp_row_0]
        del region_line_dict[temp_row_1]
        region_line_dict[';'.join([temp_row_0, temp_row_1])] = len(all_clusters)

    for row in X_arff:
        data_file += ','.join(['"' + str(int(-row[0])) + '"', ','.join(map('{:.3f}'.format, row[1:-1])),
                               '"C' + str(int(row[-1])) + '"\n'])

    cluster_file = '@RELATION Ozone_clustering\n\n'
    cluster_file += '@ATTRIBUTE name string\n'
    cluster_file += '@ATTRIBUTE left_node NUMERIC\n'
    cluster_file += '@ATTRIBUTE right_node NUMERIC\n'
    cluster_file += '@ATTRIBUTE distance NUMERIC\n'
    cluster_file += '@ATTRIBUTE class STRING\n'
    for f in important_features:
        cluster_file += '@ATTRIBUTE V' + str(f) + ' STRING\n'
    cluster_file += '@ATTRIBUTE P STRING\n'
    cluster_file += '@ATTRIBUTE P1 STRING\n'
    cluster_file += '@ATTRIBUTE P2 STRING\n'
    cluster_file += '@ATTRIBUTE P3 STRING\n'

    cluster_file += '\n@DATA\n'
    for cluster in all_clusters:
        cluster_file += cluster + '\n'

    error = abs(y_test - prediction)
    RMSE_test = np.sqrt(np.mean(error ** 2))
    print(RMSE_test, regressor.tree_.node_count)

    with open('Ozone_3.data.arff', 'w') as file:
        file.write(data_file)
    with open('Ozone_3.cluster.arff', 'w') as file:
        file.write(cluster_file)
